#pragma once
#include <unordered_map>
#include <vector>

namespace Gfx
{
namespace Spline
{

template<class T>
T BinomialCoeff(T n, T k) {
	T product = 1;

	for(auto i = 0; i < k; i++) {
		product *= (n - i) / (k - i);
	}

	return product;
}

template<class T, class Element>
Element BezierCurve(const std::vector<Element>& p, T t)
{
	auto n = p.size() - 1;

	Element result;

	for (T i = 0; i <= n; i++)
	{
		auto bi = BinomialCoeff<T>(n, i);

		result = result + p[i] * bi * powf(1 - t, n - i) * powf(t, i);
	}

	return result;
}
}
}
