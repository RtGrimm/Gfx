#include "MeshFactory.hpp"
#include "../LibTess2/Include/tesselator.h"
#include <limits>

namespace Gfx
{
struct MeshFactory::InternalData
{
	TESStesselator* Tesselator;

	~InternalData()
	{

	}
};

MeshFactory::MeshFactory(IResourceFactory& resourceFactory) :
		_ResourceFactory(resourceFactory)
{
	_Data = std::make_unique<InternalData>();
	_Data->Tesselator = tessNewTess(nullptr);
}

MeshFactory::~MeshFactory()
{
}

MeshHandle MeshFactory::BuildMesh(const Path& path)
{


	auto* tess = _Data->Tesselator;

	for (auto& figure : path.Figures)
	{
		tessAddContour(tess, 2, figure.Points.data(), sizeof(glm::vec2),
				figure.Points.size());
	}

	tessTesselate(tess, TessWindingRule::TESS_WINDING_NONZERO,
			TessElementType::TESS_POLYGONS, 3, 3, nullptr);

	auto vertexPtr = reinterpret_cast<const glm::vec3*>(tessGetVertices(tess));
	auto indexPtr = reinterpret_cast<const uint32_t*>(tessGetElements(tess));

	auto vertices = GenerateVertices(vertexPtr,
			vertexPtr + tessGetVertexCount(tess));

	Mesh desc;
	desc.Vertices = std::move(vertices);
	desc.Indices = std::vector<uint32_t>(indexPtr,
			indexPtr + tessGetElementCount(tess) * 3);

	return _ResourceFactory.CreateMesh(desc);
}

}

