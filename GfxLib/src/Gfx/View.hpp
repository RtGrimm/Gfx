#pragma once
#include <cstdint>

namespace Gfx
{
template<class T>
class View
{
public:
	View(T* data, uint32_t length) :
			_Data(data), _Length(length)
	{

	}

	View() {}

	bool IsNull() const {
		return _Data == nullptr;
	}

	explicit operator bool() const {
		return IsNull();
	}

	T* Data()
	{
		return _Data;
	}

	const T* Data() const
	{
		return _Data;
	}

	uint32_t Length() const
	{
		return _Length;
	}

private:
	T* _Data = nullptr;
	uint32_t _Length = 0;
};

template<class T>
using ConstView = View<const T>;

}
