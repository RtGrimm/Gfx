#include "PathOp.hpp"
#include "../Clipper/clipper.hpp"
#include <cstdint>
namespace Gfx
{
namespace PathOp
{

static constexpr ClipperLib::cInt ClipperScaleFactor = 100;

ClipperLib::cInt ToClipperValue(float value)
{
	return static_cast<ClipperLib::cInt>(value * ClipperScaleFactor);
}

float ToFloat(ClipperLib::cInt value)
{
	return static_cast<float>(value) / static_cast<float>(ClipperScaleFactor);
}

Polyline ToPolyline(const ClipperLib::Path& path)
{
	Polyline polyline;

	for (auto& point : path)
	{
		polyline.Points.emplace_back(ToFloat(point.X), ToFloat(point.Y));
	}

	return polyline;
}

ClipperLib::Path ToClipperPath(const Polyline& polyline)
{
	ClipperLib::Path path;

	for (auto& point : polyline.Points)
	{
		path.emplace_back(ToClipperValue(point.x), ToClipperValue(point.y));
	}

	return path;
}

Path OffsetPath(const Path& path, float offset, JoinType joinType, EndType endType)
{
	ClipperLib::ClipperOffset clipperOffset;

	for (auto& polyline : path.Figures)
	{
		clipperOffset.AddPath(ToClipperPath(polyline),
				static_cast<ClipperLib::JoinType>(joinType),
				static_cast<ClipperLib::EndType>(endType));
	}

	ClipperLib::Paths paths;
	clipperOffset.Execute(paths, offset * ClipperScaleFactor);

	Path outputPath;

	for(auto& path : paths) {
		outputPath.Figures.push_back(ToPolyline(path));
	}



	return outputPath;
}


}
}
