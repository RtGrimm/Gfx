#pragma once

#include "Brush.hpp"
#include "GLUtils.hpp"
#include "ItemBuffer.hpp"
#include "ResourceFactory.hpp"

#include <vector>
#include <string>

namespace Gfx
{
namespace GL45
{

class IBrushShader
{
public:
	virtual ~IBrushShader()
	{
	}

	virtual void BindBuffer() = 0;
	virtual void Use() = 0;

	virtual GLuint BrushUniformId() const = 0;
	virtual GLuint TransformUniformId() const = 0;

	virtual uint32_t ResolveBrushOffset(BrushRef brush) const = 0;

	virtual const GL::Handle::Program& Get() const = 0;
};

template<class Data>
class BrushShader: public IBrushShader
{
	struct HandleSelector
	{
		static const Data& Select(const BrushHandleList& handleList,
				const HandleIdType& id)
		{
			auto& value = handleList.Resolve(id);
			auto& data = value.Get<Data>();

			return data;
		}
	};

public:

	BrushShader(const BrushHandleList& handleList) :
			_Buffer(handleList)
	{

	}

	void CompileShader(const std::string& vertexShader,
			const std::string& fragmentShader)
	{
		_Program = GL::Shader::Compile(
		{
		{ GL::Shader::Type::Vertex, vertexShader },
		{ GL::Shader::Type::Fragment, fragmentShader } });

		_BrushUniformId = glGetUniformLocation(_Program.Id(), "BrushId");
		_TransformUniformId = glGetUniformLocation(_Program.Id(),
				"TransformId");
	}

	void Upload(const std::vector<BrushRef>& brushes)
	{
		_Buffer.Upload(brushes);
	}

	uint32_t ResolveBrushOffset(BrushRef brush) const
	{
		return _Buffer.Resolve(brush);
	}

	const GL::Handle::Program& Get() const
	{
		return _Program;
	}

	void Use()
	{
		glUseProgram(_Program.Id());
	}

	void BindBuffer()
	{
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER,
				RendererConst::BrushBindingPoint, _Buffer.Buffer().Id());
	}

	GLuint BrushUniformId() const
	{
		return _BrushUniformId;
	}

	GLuint TransformUniformId() const
	{
		return _TransformUniformId;
	}

private:
	GLuint _TransformUniformId = 0;
	GLuint _BrushUniformId = 0;

	using BrushBuffer = ItemBuffer<Data, BrushHandleList, BrushRef, HandleSelector>;

	BrushBuffer _Buffer;
	GL::Handle::Program _Program;

};
}
}
