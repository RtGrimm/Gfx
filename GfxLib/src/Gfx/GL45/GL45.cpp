#include "GL45.hpp"
#include "Platform.hpp"

namespace Gfx {
namespace GL45 {
std::unique_ptr<IPlatform> CreatePlatform() {
	return std::unique_ptr<IPlatform>(new Platform());
}
}
}
