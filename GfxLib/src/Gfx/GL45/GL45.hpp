#pragma once
#include <memory>
#include "../Gfx.hpp"

namespace Gfx {
namespace GL45 {
	std::unique_ptr<IPlatform> CreatePlatform();
}
}
