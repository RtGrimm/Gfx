#pragma once

#include "GLUtils.hpp"
#include "Texture.hpp"
#include "ResourceFactory.hpp"
#include "CommandList.hpp"
#include "ItemBuffer.hpp"
#include "Buffer.hpp"
#include "../Gfx.hpp"
#include <vector>

namespace Gfx
{
namespace GL45
{
class ImageRenderer
{
	static constexpr const char* VertexShader =
			R"GLSL(
#version 450 core

layout(location = 0) in vec3 VertLocation;
layout(location = 1) in vec2 VertUV;

layout(location = 2) in int ImageIndex;
layout(location = 3) in int TransformIndex;

layout(std430, binding=1) buffer TransformData {
    mat4 Transforms[];
};

out vec2 FragUV;
flat out int FragImageIndex; 

void main() {
    gl_Position = Transforms[TransformIndex] * vec4(VertLocation, 1);

    FragImageIndex = ImageIndex;
    FragUV = VertUV;
}

)GLSL";

	static constexpr const char* FragmentShader =
			R"GLSL(
#version 450 core
#extension GL_ARB_bindless_texture : require

out vec4 Color;

in vec2 FragUV;
flat in int FragImageIndex; 

layout(std430, binding=2) buffer ImageData {
    sampler2D Images[];
};

void main() {
    Color = texture(Images[FragImageIndex], FragUV);
}

)GLSL";

	struct HandleSelector
	{
		static GLuint64 Select(const TextureHandleList& handleList,
				const HandleIdType& id)
		{
			auto& value = handleList.Resolve(id);
			return value.BindlessHandle();
		}

	};

	struct DrawIndex
	{
		int32_t ImageIndex = 0;
		int32_t TransformIndex = 0;

		DrawIndex(int32_t imageIndex, int32_t transformIndex) :
				ImageIndex(imageIndex), TransformIndex(transformIndex)
		{

		}
	};

public:
	ImageRenderer(TextureHandleList& textureList,
			const TransformHandleList& transformList) :
			_TransformBuffer(transformList), _TextureHandleBuffer(textureList), _TextureList(
					textureList)
	{
		InitQuad();
		InitVertexArray();
	}

	void Render(const RefVector<const Command>& commands)
	{
		if(commands.size() == 0)
			return;

		UploadData(commands);
		UpdateIndices(commands);

		glBindVertexArray(_VertexArray.Id());

		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1,
				_TransformBuffer.Buffer().Id());

		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2,
				_TextureHandleBuffer.Buffer().Id());

		glUseProgram(_Program.Id());

		glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, commands.size());
	}

private:
	using TransformBuffer = ItemBuffer<glm::mat4, TransformHandleList, TransformRef>;
	using TextureHandleBuffer = ItemBuffer<GLuint64, TextureHandleList, TextureRef, HandleSelector>;

	TransformBuffer _TransformBuffer;
	TextureHandleBuffer _TextureHandleBuffer;

	TextureHandleList& _TextureList;

	Buffer<DrawIndex> _DrawIndexBuffer;

	GL::Handle::Buffer _QuadMeshBuffer;
	GL::Handle::VertexArray _VertexArray;

	GL::Handle::Program _Program = GL::Shader::Compile(
	{
	{ GL::Shader::Type::Vertex, VertexShader },
	{ GL::Shader::Type::Fragment, FragmentShader } });

	void UpdateIndices(const RefVector<const Command>& commands)
	{
		_DrawIndexBuffer.Resize(commands.size());
		_DrawIndexBuffer.Map();

		auto ptr = _DrawIndexBuffer.Ptr();

		for (auto& command : commands)
		{
			auto& imageCommand = command.get().Get<ImageCommandData>();

			ptr->TransformIndex = _TransformBuffer.Resolve(
					imageCommand.Transform);

			ptr->ImageIndex = _TextureHandleBuffer.Resolve(
					imageCommand.Texture);

			ptr++;
		}

		_DrawIndexBuffer.Unmap();
	}

	void UploadData(const RefVector<const Command>& commands)
	{
		std::vector<TransformRef> transforms;
		std::vector<TextureRef> textures;

		for (auto& meshCommand : commands)
		{
			auto& command = meshCommand.get().Get<ImageCommandData>();

			transforms.push_back(command.Transform);
			textures.push_back(command.Texture);
		}

		_TransformBuffer.Upload(transforms);
		_TextureHandleBuffer.Upload(textures);

	}

	void InitQuad()
	{
		_QuadMeshBuffer = GL::Handle::Buffer::Create();

		std::vector<Vertex> vertices
		{
		{
		{ -1, 1, 0 },
		{ 0, 0 } },
		{
		{ 1, 1, 0 },
		{ 1, 0 } },
		{
		{ -1, -1, 0 },
		{ 0, 1 } },
		{
		{ 1, -1, 0 },
		{ 1, 1 } } };

		glNamedBufferData(_QuadMeshBuffer.Id(), sizeof(Vertex) * 4,
				vertices.data(),
				GL_STATIC_DRAW);
	}

	void InitVertexArray()
	{
		_VertexArray = GL::Handle::VertexArray::Create();

		auto locationAttribLocation = 0;
		auto uvAttribLocation = 1;

		auto imageIndexAttribLocation = 2;
		auto transformIndexAttribLocation = 3;

		glEnableVertexArrayAttrib(_VertexArray.Id(), locationAttribLocation);
		glEnableVertexArrayAttrib(_VertexArray.Id(), uvAttribLocation);

		glEnableVertexArrayAttrib(_VertexArray.Id(), imageIndexAttribLocation);
		glEnableVertexArrayAttrib(_VertexArray.Id(),
				transformIndexAttribLocation);

		glVertexArrayAttribFormat(_VertexArray.Id(), locationAttribLocation, 3,
		GL_FLOAT, false, 0);
		glVertexArrayAttribFormat(_VertexArray.Id(), uvAttribLocation, 2,
		GL_FLOAT, false, sizeof(glm::vec3));

		glVertexArrayAttribIFormat(_VertexArray.Id(), imageIndexAttribLocation,
				1,
				GL_INT, 0);
		glVertexArrayAttribIFormat(_VertexArray.Id(),
				transformIndexAttribLocation, 1,
				GL_INT, sizeof(int32_t));

		glVertexArrayVertexBuffer(_VertexArray.Id(), 0, _QuadMeshBuffer.Id(), 0,
				sizeof(Vertex));

		glVertexArrayVertexBuffer(_VertexArray.Id(), 1,
				_DrawIndexBuffer.Handle().Id(), 0, sizeof(DrawIndex));

		glVertexArrayAttribBinding(_VertexArray.Id(), locationAttribLocation,
				0);
		glVertexArrayAttribBinding(_VertexArray.Id(), uvAttribLocation, 0);

		glVertexArrayAttribBinding(_VertexArray.Id(), imageIndexAttribLocation,
				1);
		glVertexArrayAttribBinding(_VertexArray.Id(),
				transformIndexAttribLocation, 1);

		glVertexArrayBindingDivisor(_VertexArray.Id(), 0, 0);
		glVertexArrayBindingDivisor(_VertexArray.Id(), 1, 1);
	}

};
}
}
