#pragma once
#include "GLUtils.hpp"
#include "ResourceFactory.hpp"
#include "MeshBuffer.hpp"
#include "MeshBrushManager.hpp"
#include "CommandList.hpp"

#include <vector>
#include <unordered_set>

namespace Gfx
{
namespace GL45
{

class MeshRenderer
{

	struct DrawElementsIndirectCommand
	{
		uint count;
		uint primCount;
		uint firstIndex;
		uint baseVertex;
		uint baseInstance;
	};

	struct DrawIndex
	{
		int32_t BrushIndex = 0;
		int32_t TransformIndex = 0;

		DrawIndex(int32_t brushIndex, int32_t transformIndex) :
				BrushIndex(brushIndex), TransformIndex(transformIndex)
		{

		}
	};

public:

	MeshRenderer(const MeshHandleList& meshList,
			const TransformHandleList& transformList,
			const BrushHandleList& brushList) :
			_BrushList(brushList), _MeshList(meshList), _MeshBuffer(meshList), _TransformBuffer(
					transformList/*, _TransformResolveCallback*/), _BrushManager(
					brushList)
	{
		InitBuffer();
	}

	void Render(const RefVector<const Command>& commands)
	{
		if (commands.size() == 0)
			return;

		UploadData(commands);

		glBindBufferBase(GL_SHADER_STORAGE_BUFFER,
				RendererConst::TransformBindingPoint,
				_TransformBuffer.Buffer().Id());

		glBindBuffer(GL_DRAW_INDIRECT_BUFFER, _CommandBuffer.Handle().Id());

		glBindVertexArray(_VertexArray.Id());

		std::unordered_map<int32_t, RefVector<const Command>> commandMap;

		for (auto& command : commands)
		{
			auto& brush = _BrushList.Resolve(
					command.get().Get<MeshCommandData>().Brush);
			commandMap[brush.Index()].push_back(command);
		}

		for (auto& pair : commandMap)
		{
			RenderItems(pair.second, _BrushManager.GetBrushShader(pair.first));
		}

		glBindVertexArray(0);
		glUseProgram(0);
	}

private:
	using TransformBuffer = ItemBuffer<glm::mat4, TransformHandleList, TransformRef>;

	/*TransformBuffer::ResolveHandleCallback _TransformResolveCallback =
	 [] (const TransformHandleList& handleList, const HandleIdType& id) -> const glm::mat4&
	 {
	 auto& value = handleList.Resolve(id);
	 return value;
	 };*/

	const BrushHandleList& _BrushList;
	const MeshHandleList& _MeshList;

	MeshBuffer _MeshBuffer;
	TransformBuffer _TransformBuffer;
	GL::Handle::VertexArray _VertexArray;

	BrushManager _BrushManager;

	Buffer<DrawElementsIndirectCommand> _CommandBuffer;
	Buffer<DrawIndex> _DrawIndexBuffer;

	void RenderItems(const RefVector<const Command>& commands,
			IBrushShader& brush)
	{

		UpdateIndices(commands, brush);
		UpdateIndirectCommands(commands);

		brush.Use();
		brush.BindBuffer();

		glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, nullptr,
				commands.size(), sizeof(DrawElementsIndirectCommand));

	}

	void UpdateIndirectCommands(const RefVector<const Command>& commands)
	{
		_CommandBuffer.Resize(commands.size());
		_CommandBuffer.Map();

		auto ptr = _CommandBuffer.Ptr();

		int32_t i = 0;

		for (auto& command : commands)
		{
			auto& meshCommand = command.get().Get<MeshCommandData>();

			auto& mesh = _MeshList.Resolve(meshCommand.Mesh);

			auto indexOffset = _MeshBuffer.IndexOffset(meshCommand.Mesh);
			auto vertexOffset = _MeshBuffer.VertexOffset(meshCommand.Mesh);

			ptr->count = mesh.Indices.size();
			ptr->firstIndex = indexOffset;
			ptr->primCount = 1;
			ptr->baseVertex = vertexOffset;
			ptr->baseInstance = i;

			i++;

			ptr++;
		}

		_CommandBuffer.Unmap();
	}

	void UpdateIndices(const RefVector<const Command>& commands,
			const IBrushShader& brush)
	{
		_DrawIndexBuffer.Resize(commands.size());
		_DrawIndexBuffer.Map();

		auto ptr = _DrawIndexBuffer.Ptr();

		for (auto& command : commands)
		{
			auto& meshCommand = command.get().Get<MeshCommandData>();

			ptr->TransformIndex = _TransformBuffer.Resolve(
					meshCommand.Transform);

			ptr->BrushIndex = brush.ResolveBrushOffset(meshCommand.Brush);

			ptr++;
		}

		_DrawIndexBuffer.Unmap();
	}

	void UploadData(const RefVector<const Command>& commands)
	{
		std::vector<MeshRef> meshes;
		std::vector<TransformRef> transforms;
		std::vector<BrushRef> brushes;

		for (auto& meshCommand : commands)
		{
			auto& command = meshCommand.get().Get<MeshCommandData>();

			meshes.push_back(command.Mesh);
			transforms.push_back(command.Transform);
			brushes.push_back(command.Brush);
		}

		_MeshBuffer.Upload(meshes);
		_TransformBuffer.Upload(transforms);
		_BrushManager.Upload(brushes);
	}

	void InitBuffer()
	{

		_VertexArray = GL::Handle::VertexArray::Create();

		auto locationAttribLocation = 0;
		auto uvAttribLocation = 1;

		auto brushIndexAttribLocation = 2;
		auto transformIndexAttribLocation = 3;

		glEnableVertexArrayAttrib(_VertexArray.Id(), locationAttribLocation);
		glEnableVertexArrayAttrib(_VertexArray.Id(), uvAttribLocation);

		glEnableVertexArrayAttrib(_VertexArray.Id(), brushIndexAttribLocation);
		glEnableVertexArrayAttrib(_VertexArray.Id(),
				transformIndexAttribLocation);

		glVertexArrayAttribFormat(_VertexArray.Id(), locationAttribLocation, 3,
		GL_FLOAT, false, 0);
		glVertexArrayAttribFormat(_VertexArray.Id(), uvAttribLocation, 2,
		GL_FLOAT, false, sizeof(glm::vec3));

		glVertexArrayAttribIFormat(_VertexArray.Id(), brushIndexAttribLocation,
				1,
				GL_INT, 0);
		glVertexArrayAttribIFormat(_VertexArray.Id(),
				transformIndexAttribLocation, 1,
				GL_INT, sizeof(int32_t));

		glVertexArrayElementBuffer(_VertexArray.Id(),
				_MeshBuffer.IndexBuffer().Id());

		glVertexArrayVertexBuffer(_VertexArray.Id(), 0,
				_MeshBuffer.VertexBuffer().Id(), 0, sizeof(Vertex));

		glVertexArrayVertexBuffer(_VertexArray.Id(), 1,
				_DrawIndexBuffer.Handle().Id(), 0, sizeof(DrawIndex));

		glVertexArrayAttribBinding(_VertexArray.Id(), locationAttribLocation,
				0);
		glVertexArrayAttribBinding(_VertexArray.Id(), uvAttribLocation, 0);

		glVertexArrayAttribBinding(_VertexArray.Id(), brushIndexAttribLocation,
				1);
		glVertexArrayAttribBinding(_VertexArray.Id(),
				transformIndexAttribLocation, 1);

		glVertexArrayBindingDivisor(_VertexArray.Id(), 0, 0);
		glVertexArrayBindingDivisor(_VertexArray.Id(), 1, 1);
	}
};
}
}
