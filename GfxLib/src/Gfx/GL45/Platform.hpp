#pragma once

#include "ImageRenderer.hpp"
#include "MeshRenderer.hpp"
#include "MeshBuffer.hpp"
#include "CommandList.hpp"
#include "BrushShader.hpp"
#include "ItemBuffer.hpp"
#include "BufferControllers.hpp"
#include "Buffer.hpp"
#include "ResourceFactory.hpp"
#include "Brush.hpp"
#include "RendererConst.hpp"
#include "MeshBrushManager.hpp"

#include <glm/glm.hpp>
#include <vector>
#include <memory>
#include <algorithm>
#include <unordered_set>
#include <set>
#include <functional>
#include "../Gfx.hpp"
#include "GLUtils.hpp"
#include "../HandleList.hpp"
#include "../Allocators.hpp"

namespace Gfx
{
namespace GL45
{
class CommandRunner: public ICommandRunner
{
public:
	CommandRunner(MeshRenderer& renderer, ImageRenderer& imageRenderer) :
			_Renderer(renderer), _ImageRenderer(imageRenderer)
	{

	}

	void Run(const RenderState& renderState, const ICommandList& commandList) override
	{
		GL::Error::Check();

		glClearColor(renderState.ClearColor.r, renderState.ClearColor.g,
				renderState.ClearColor.b, renderState.ClearColor.a);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glViewport(0, 0, renderState.Size.x, renderState.Size.y);

		auto commandListImpl = dynamic_cast<const CommandList&>(commandList);
		_Renderer.Render(commandListImpl.CommandsOfType<MeshCommandData>());

		_ImageRenderer.Render(
				commandListImpl.CommandsOfType<ImageCommandData>());
	}

private:
	MeshRenderer& _Renderer;
	ImageRenderer& _ImageRenderer;
};

class Platform: public IPlatform
{
public:
	Platform() :
			_MeshRenderer(_ResourceFactory.MeshList(),
					_ResourceFactory.TransformList(),
					_ResourceFactory.BrushList()), _ImageRenderer(
					_ResourceFactory.TextureList(),
					_ResourceFactory.TransformList()), _Runner(_MeshRenderer,
					_ImageRenderer)
	{

	}

	ICommandRunner& GetCommandRunner() override
	{
		return _Runner;
	}

	std::unique_ptr<ICommandList> CreateCommandList() override
	{
		auto commandList = new CommandList();

		return std::unique_ptr<ICommandList>(commandList);
	}

	IResourceFactory& GetResourceFactory() override
	{
		return _ResourceFactory;
	}

private:
	ResourceFactory _ResourceFactory;

	MeshRenderer _MeshRenderer;
	ImageRenderer _ImageRenderer;
	CommandRunner _Runner;

};

}
}
