#pragma once
#include "ResourceFactory.hpp"
#include "BufferControllers.hpp"
#include <unordered_set>
#include <iostream>

namespace Gfx
{
namespace GL45
{
class MeshBuffer
{
	struct MeshPtr
	{
		uint32_t VertexCount = 0;
		uint32_t VertexOffset = 0;

		uint32_t IndexCount = 0;
		uint32_t IndexOffset = 0;

		uint32_t TransformOffset = 0;

		MeshPtr()
		{
		}
	};

public:
	MeshBuffer(const MeshHandleList& handleList) :
			_HandleList(handleList), _VertexBufferController(256), _IndexBufferController(
					256)
	{

	}

	void Upload(const std::vector<MeshRef>& handles)
	{
		EnsureSize(handles);

		std::unordered_set<HandleIdType> uploadHandles;

		for (auto& meshHandle : handles)
		{
			uploadHandles.insert(std::end(uploadHandles), meshHandle.Id());
		}

		auto it = std::begin(_CurrentMeshes);

		while(it != std::end(_CurrentMeshes))
		{
			auto currentMesh = *it;

			auto uploadHandleIt = uploadHandles.find(currentMesh);

			if (uploadHandleIt == std::end(uploadHandles))
			{
				it = _CurrentMeshes.erase(it);

				auto ptr = _MeshPtrList[currentMesh];

				_VertexBufferController.Free(ptr.VertexOffset, ptr.VertexCount);
				_IndexBufferController.Free(ptr.IndexOffset, ptr.IndexCount);
				_MeshPtrList.erase(_MeshPtrList.find(currentMesh));

				if (it == std::end(_CurrentMeshes))
					break;
			}
			else
			{
				it++;
				uploadHandles.erase(uploadHandleIt);
			}
		}

		UploadMeshes(uploadHandles);
	}

	uint32_t IndexOffset(MeshRef mesh) const
	{
		return _MeshPtrList.at(mesh.Id()).IndexOffset;
	}

	uint32_t VertexOffset(MeshRef mesh) const
	{
		return _MeshPtrList.at(mesh.Id()).VertexOffset;
	}

	const GL::Handle::Buffer& VertexBuffer() const
	{
		return _VertexBufferController.Handle();
	}

	const GL::Handle::Buffer& IndexBuffer() const
	{
		return _IndexBufferController.Handle();
	}

private:
	const MeshHandleList& _HandleList;

	ContiguousBufferController<TreeBuddyAllocator, Vertex> _VertexBufferController;
	ContiguousBufferController<TreeBuddyAllocator, uint32_t> _IndexBufferController;

	std::unordered_set<HandleIdType> _CurrentMeshes;
	std::unordered_map<HandleIdType, MeshPtr> _MeshPtrList;

	void EnsureSize(const std::vector<MeshRef>& handles)
	{
		auto roundSize = [] (uint32_t size) -> uint32_t
		{
			return static_cast<uint32_t>(pow(2, ceil(log2(size))));
		};

		uint32_t totalVertexCount = 0;
		uint32_t totalIndexCount = 0;

		for (auto& handle : handles)
		{
			auto& mesh = _HandleList.Resolve(handle);

			totalIndexCount += roundSize(mesh.Indices.size());
			totalVertexCount += roundSize(mesh.Vertices.size());
		}

		auto clear = [&]
		{
			_CurrentMeshes.clear();
			_MeshPtrList.clear();
			_VertexBufferController.ClearAllocator();
			_IndexBufferController.ClearAllocator();
		};

		auto roundVertexCount = roundSize(totalVertexCount);
		auto roundIndexCount = roundSize(totalIndexCount);

		if (_VertexBufferController.MaxCount() < roundVertexCount)
		{
			_VertexBufferController.Resize(roundVertexCount);
			clear();
		}

		if (_IndexBufferController.MaxCount() < roundIndexCount)
		{
			_IndexBufferController.Resize(roundIndexCount);
			clear();
		}
	}

	void UploadMeshes(const std::unordered_set<HandleIdType>& meshIds)
	{
		if (meshIds.size() == 0)
			return;

		_VertexBufferController.Map();
		_IndexBufferController.Map();

		for (auto& id : meshIds)
		{
			auto& mesh = _HandleList.Resolve(id);

			auto& ptr = _MeshPtrList[id];

			ptr.IndexCount = mesh.Indices.size();
			ptr.VertexCount = mesh.Vertices.size();

			ptr.VertexOffset = _VertexBufferController.Alloc(ptr.VertexCount);
			ptr.IndexOffset = _IndexBufferController.Alloc(ptr.IndexCount);

			auto vertexPtr = _VertexBufferController.Resolve(ptr.VertexOffset);
			auto indexPtr = _IndexBufferController.Resolve(ptr.IndexOffset);

			std::copy(std::begin(mesh.Vertices), std::end(mesh.Vertices),
					vertexPtr);
			std::copy(std::begin(mesh.Indices), std::end(mesh.Indices),
					indexPtr);

			_CurrentMeshes.insert(id);
		}

		_IndexBufferController.Unmap();
		_VertexBufferController.Unmap();
	}
};
}
}
