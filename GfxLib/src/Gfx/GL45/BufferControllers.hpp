#pragma once
#include "../Allocators.hpp"
#include <cstdint>
#include "Buffer.hpp"

namespace Gfx {
namespace GL45 {
template<template<class Unit> class Allocator, class T>
class ContiguousBufferController
{
public:
	ContiguousBufferController(uint32_t count)
	{
		Resize(count);
	}

	T* Resolve(uint32_t offset)
	{
		return _Buffer.Ptr() + offset;
	}

	void Resize(uint32_t count)
	{
		_Buffer.Resize(count);
		_Allocator.Resize(count);
	}

	void ClearAllocator() {
		_Allocator.Clear();
	}

	void Map()
	{
		_Buffer.Map();
	}

	void Unmap()
	{
		_Buffer.Unmap();
	}

	uint32_t Alloc(uint32_t count)
	{
		return _Allocator.Alloc(count);
	}

	void Free(uint32_t offset, uint32_t count)
	{
		_Allocator.Free(offset, count);
	}

	uint32_t MaxCount() const
	{
		return _Buffer.MaxCount();
	}

	const GL::Handle::Buffer& Handle() const
	{
		return _Buffer.Handle();
	}

private:
	Buffer<T> _Buffer;

	Allocator<uint32_t> _Allocator;
};

template<template<class Unit> class Allocator, class T>
class ItemBufferController
{
public:
	ItemBufferController(uint32_t count)
	{
		Resize(count);
	}

	T* Resolve(uint32_t offset)
	{
		return _Buffer.Ptr() + offset;
	}

	void Resize(uint32_t count)
	{
		_Buffer.Resize(count);
		_Allocator.Resize(count);
	}

	std::vector<uint32_t> Alloc(uint32_t count)
	{
		return _Allocator.Alloc(count);
	}

	uint32_t Alloc()
	{
		return _Allocator.Alloc();
	}

	void Free(uint32_t offset)
	{
		_Allocator.Free(offset);
	}

	void Map()
	{
		_Buffer.Map();
	}

	void Unmap()
	{
		_Buffer.Unmap();
	}

	uint32_t MaxCount() const
	{
		return _Buffer.MaxCount();
	}

	const GL::Handle::Buffer& Handle() const
	{
		return _Buffer.Handle();
	}

private:
	Buffer<T> _Buffer;
	Allocator<uint32_t> _Allocator;
};
}
}
