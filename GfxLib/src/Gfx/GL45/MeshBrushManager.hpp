#pragma once
#include <string>
#include "ResourceFactory.hpp"
#include "BrushShader.hpp"

namespace Gfx
{
namespace GL45
{
class MeshShaders
{
	static constexpr const char* FragmentBase =
			R"GLSL(
	#version 450 core
	flat in int BrushId;

)GLSL";

	static constexpr const char* GradientBase =
			R"GLSL(
	const int StopCount = 16;
	
	struct GradientStopData {
		float[StopCount] StopOffsets;
		vec4[StopCount] StopColors;
	
		int StopCount;
		int _Padding_1; int _Padding_2; int _Padding_3;
	};
	
	vec4 GenerateGradient(float offset, GradientStopData data) {
		vec4 color = vec4(0, 0, 0, 0);
	
		for(int i = 1; i < StopCount; i++) {
		   float lowerOffset = data.StopOffsets[i - 1];
		   float upperOffset = data.StopOffsets[i];
	
		   float localOffset = (offset - lowerOffset) 
						  / (upperOffset - lowerOffset);
	
		   vec4 newColor = mix(data.StopColors[i - 1], data.StopColors[i], localOffset);
		   color = mix(color, newColor, offset >= lowerOffset && offset <= upperOffset);
		}
	
		color = mix(color, data.StopColors[data.StopCount - 1], 
					offset >= data.StopOffsets[data.StopCount - 1]);

        color = mix(color, data.StopColors[0], 
					offset <= data.StopOffsets[0]);
	
		return color;
	}
)GLSL";

	static constexpr const char* VertexShader =
			R"GLSL(
	#version 450 core

	layout(location = 0) in vec3 Location;
	layout(location = 1) in vec2 UV;
    layout(location = 2) in int BrushIndex;
    layout(location = 3) in int TransformIndex;

    out vec2 FragUV;

    flat out int BrushId;

	layout(std430, binding=1) buffer TransformBuffer {
	    mat4 Transform[];
	};


	void main() {
	    gl_Position = Transform[TransformIndex] * vec4(Location, 1);
        FragUV = UV;
        BrushId = BrushIndex;
	}
	)GLSL";

	static constexpr const char* SolidColorFragmentShader =
			R"GLSL(
	layout(std430, binding=2) buffer BrushBuffer {
		vec4 ColorList[];
	};

	out vec4 Color;
    in vec2 FragUV;

	void main() {
		Color = ColorList[BrushId];
        
	}
	)GLSL";

	static constexpr const char* LinearGradientFragmentShader =
			R"GLSL(
	    struct Data {
	        vec2 Start;
			vec2 End;
 
			GradientStopData StopData;
	    };

		layout(std430, binding=2) buffer BrushBuffer {
		    Data DataList[];
		};

	    in vec2 FragUV;

		out vec4 Color;

	  
		void main() {
	        Data data = DataList[BrushId];        

			vec2 startPoint = data.Start;
			vec2 endPoint = data.End;
		
			vec2 v = endPoint - startPoint;
			float d = length(v);
			v = normalize(v);
		
			vec2 v0 = FragUV - startPoint;
			float offset = dot(v0, v);
			offset = offset / d;

	        Color = GenerateGradient(offset, data.StopData);
		}
		)GLSL";

	static constexpr const char* RadialGradientFragmentShader =
			R"GLSL(
	    struct Data {
	        vec2 Center;
			vec2 Radius;
 
			GradientStopData StopData;
	    };

		layout(std430, binding=2) buffer BrushBuffer {
		    Data DataList[];
		};

	    in vec2 FragUV;

		out vec4 Color;

	  
		void main() {
	        Data data = DataList[BrushId];        

	        float offset = length(
	            (FragUV - data.Center) / data.Radius);

	        Color = GenerateGradient(offset, data.StopData);
		}
	)GLSL";

public:

	static std::string BuildVertexShader()
	{
		return std::string(VertexShader);
	}

	static std::string BuildSolidFragmentShader()
	{
		return std::string(FragmentBase) + SolidColorFragmentShader;
	}

	static std::string BuildLinearGradientShader()
	{
		return std::string(FragmentBase) + GradientBase
				+ LinearGradientFragmentShader;
	}

	static std::string BuildRadialGradientShader()
	{
		return std::string(FragmentBase) + GradientBase
				+ RadialGradientFragmentShader;
	}
};

class BrushManager
{
public:
	BrushManager(const BrushHandleList& brushList) :
			_BrushList(brushList), _SolidColorBrush(brushList), _RadialGradientBrush(
					brushList), _LinearGradientBrush(brushList)
	{
		BuildShaders();
	}

	IBrushShader& GetBrushShader(int32_t type)
	{
		if (type == Brush::IndexOfType<SolidColorData>)
		 return _SolidColorBrush;

		 if (type == Brush::IndexOfType<RadialGradientData>)
		 return _RadialGradientBrush;

		 if (type == Brush::IndexOfType<LinearGradientData>)
		 return _LinearGradientBrush;

		 throw std::runtime_error("Invalid brush type.");
	}

	void Upload(const std::vector<BrushRef>& brushes)
	{
		std::vector<BrushRef> solidBrushes;
		std::vector<BrushRef> radialBrushes;
		std::vector<BrushRef> linearBrushes;

		for (auto brushRef : brushes)
		{

			auto& brush = _BrushList.Resolve(brushRef);

			brush.Match(Case < SolidColorData > ([&] (const SolidColorData&)
			{
				solidBrushes.push_back(brushRef);
			}), Case < LinearGradientData > ([&] (const LinearGradientData&)
			{
				linearBrushes.push_back(brushRef);
			}), Case < RadialGradientData > ([&] (const RadialGradientData&)
			{
				radialBrushes.push_back(brushRef);
			}));
		}

		_SolidColorBrush.Upload(solidBrushes);
		_RadialGradientBrush.Upload(radialBrushes);
		_LinearGradientBrush.Upload(linearBrushes);
	}

private:
	const BrushHandleList& _BrushList;

	void BuildShaders()
	{
		_SolidColorBrush.CompileShader(MeshShaders::BuildVertexShader(),
				MeshShaders::BuildSolidFragmentShader());

		_LinearGradientBrush.CompileShader(MeshShaders::BuildVertexShader(),
				MeshShaders::BuildLinearGradientShader());

		_RadialGradientBrush.CompileShader(MeshShaders::BuildVertexShader(),
				MeshShaders::BuildRadialGradientShader());
	}

	BrushShader<SolidColorData> _SolidColorBrush;
	BrushShader<RadialGradientData> _RadialGradientBrush;
	BrushShader<LinearGradientData> _LinearGradientBrush;
};
}
}
