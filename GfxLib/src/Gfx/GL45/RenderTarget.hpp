#pragma once
#include "../Union.hpp"
#include "GLUtils.hpp"

namespace Gfx
{
namespace GL45
{
struct FramebufferRenderTarget
{
	GL::Handle::Framebuffer Framebuffer;
};

struct ScreenRenderTarget
{
};

using RenderTarget = Union<FramebufferRenderTarget, ScreenRenderTarget>;

}
}
