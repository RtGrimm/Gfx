#pragma once
#include <utility>
#include <vector>
#include "ResourceFactory.hpp"
#include "../Union.hpp"

namespace Gfx
{
namespace GL45
{
template<class T>
using RefVector = std::vector<std::reference_wrapper<T>>;

struct MeshCommandData
{
	MeshRef Mesh;
	BrushRef Brush;
	TransformRef Transform;

	MeshCommandData(MeshRef mesh, BrushRef brush, TransformRef transfrom) :
			Mesh(mesh), Brush(brush), Transform(transfrom)
	{
	}
};

struct ImageCommandData
{
	TextureRef Texture;
	TransformRef Transform;

	ImageCommandData(TextureRef texture, TransformRef transform) :
			Texture(texture), Transform(transform)
	{

	}
};

using Command = Union<MeshCommandData, ImageCommandData>;

class CommandList: public ICommandList
{
public:
	CommandList() {

	}

	~CommandList() {

	}

	void DrawMesh(MeshRef mesh, BrushRef brush, TransformRef transform) override
	{
		_Commands.emplace_back(MeshCommandData(mesh, brush, transform));
	}

	void DrawImage(TextureRef image, TransformRef transform) override {
		_Commands.emplace_back(ImageCommandData(image, transform));
	}

	template<class T>
	RefVector<const Command> CommandsOfType() const
	{
		RefVector<const Command> commands;

		for (auto& command : _Commands)
		{
			if (command.Is<T>())
			{
				commands.emplace_back(command);
			}
		}

		return commands;
	}

	void Clear() override
	{
		_Commands.clear();
	}

	const std::vector<Command>& Commands() const
	{
		return _Commands;
	}

private:
	std::vector<Command> _Commands;
};
}
}
