#pragma once
#include "GLUtils.hpp"

namespace Gfx {
namespace GL45 {
template<class T>
class Buffer
{
public:
	void Map()
	{
		_Ptr = static_cast<T*>(glMapNamedBuffer(_Buffer.Id(), GL_WRITE_ONLY));
	}

	void Unmap()
	{
		glUnmapNamedBuffer(_Buffer.Id());
		_Ptr = nullptr;
	}

	const GL::Handle::Buffer& Handle() const
	{
		return _Buffer;
	}

	T* Ptr() const
	{
		return _Ptr;
	}

	uint32_t MaxCount() const
	{
		return _MaxCount;
	}

	void Resize(uint32_t count)
	{
		auto newByteSize = count * sizeof(T);

		if (count > _MaxCount)
		{
			glNamedBufferData(_Buffer.Id(), newByteSize, nullptr,
			GL_STREAM_DRAW);
			_MaxCount = count;
		}
	}

private:
	T* _Ptr = nullptr;

	GL::Handle::Buffer _Buffer = GL::Handle::Buffer::Create();
	uint32_t _MaxCount;
};

}
}
