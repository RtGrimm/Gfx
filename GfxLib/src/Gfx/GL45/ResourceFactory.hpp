#pragma once
#include "../Gfx.hpp"
#include "Brush.hpp"
#include "RenderTarget.hpp"
#include "Texture.hpp"
#include <algorithm>
#include <vector>

namespace Gfx
{
namespace GL45
{
using MeshHandleList = HandleList<MeshTag, Mesh, IncIdController>;
using BrushHandleList = HandleList<BrushTag, Brush, IncIdController>;
using TransformHandleList = HandleList<TransformTag, glm::mat4, IncIdController>;
using RenderTargetHandleList = HandleList<RenderTargetTag, RenderTarget, IncIdController>;

class ResourceFactory: public IResourceFactory
{
public:
	ResourceFactory()
	{
		_ScreenRenderTarget = _RenderTargetList.Create(ScreenRenderTarget());
	}

	RenderTargetRef GetScreenRenderTarget()
	{
		return _ScreenRenderTarget;
	}

	RenderTargetHandle CreateRenderTarget(const RenderTargetDesc& desc)
	{

	}

	TextureHandle CreateTexture(const TextureDesc& desc)
	{
		return _TextureResourceManager.Create(desc);
	}

	MeshHandle CreateMesh(Mesh& desc)
	{
		return _MeshList.Create(std::move(desc.Vertices),
				std::move(desc.Indices));
	}

	TransformHandle CreateTransform(const glm::mat4& transform)
	{
		return _TransformList.Create(transform);
	}

	BrushHandle CreateSolidBrush(glm::vec4 color)
	{
		return _BrushList.Create(SolidColorData(color));
	}

	BrushHandle CreateRadialGradientBrush(const RadialGradient& gradient)
	{
		RadialGradientData data;
		data.Center = gradient.Center;
		data.Radius = gradient.Radius;

		CopyStopData(gradient.Stops, data.StopData);

		return _BrushList.Create(data);
	}

	BrushHandle CreateLinearGradientBrush(const LinearGradient& gradient)
	{
		LinearGradientData data;
		data.Start = gradient.Start;
		data.End = gradient.End;

		CopyStopData(gradient.Stops, data.StopData);

		return _BrushList.Create(data);
	}

	const BrushHandleList& BrushList() const
	{
		return _BrushList;
	}

	const MeshHandleList& MeshList() const
	{
		return _MeshList;
	}

	const TransformHandleList& TransformList() const
	{
		return _TransformList;
	}

	TextureHandleList& TextureList()
	{
		return _TextureResourceManager.HandleList();
	}

	const TextureHandleList& TextureList() const
	{
		return _TextureResourceManager.HandleList();
	}

	const RenderTargetHandleList& RenderTargetList() const
	{
		return _RenderTargetList;
	}

private:
	BrushHandleList _BrushList;
	MeshHandleList _MeshList;
	TransformHandleList _TransformList;
	RenderTargetHandleList _RenderTargetList;

	TextureResourceManager _TextureResourceManager;

	RenderTargetHandle _ScreenRenderTarget;

	void CopyStopData(const std::vector<GradientStop>& gradientStops,
			GradientStopData& data)
	{
		if (gradientStops.size() > RendererConst::MaxGradientStops)
		{
			throw std::runtime_error("Brush exceeds max stop count.");
		}

		data.StopCount = gradientStops.size();
		std::fill(std::begin(data.StopOffsets), std::end(data.StopOffsets), -1);

		auto stops = gradientStops;

		std::sort(std::begin(stops), std::end(stops),
				[] (const GradientStop& left, const GradientStop& right)
				{
					return left.Offset < right.Offset;
				});

		std::transform(std::begin(stops), std::end(stops),
				std::begin(data.StopColors), [] (const GradientStop& stop)
				{
					return stop.Color;
				});

		std::transform(std::begin(stops), std::end(stops),
				std::begin(data.StopOffsets), [] (const GradientStop& stop)
				{
					return stop.Offset;
				});
	}

};
}
}
