#pragma once
#include <GL/glew.h>
#include <vector>
#include <stdexcept>

namespace GL {
namespace Handle {

template<class Resource>
class HandleImpl {
public:
	explicit HandleImpl(GLint id) :
			_Id(id), _Valid(true) {
	}
	HandleImpl() {
	}

	~HandleImpl() {
		Reset();
	}

	template<class ... Args>
	static HandleImpl Create(Args ... args) {
		return HandleImpl(Resource::Create(args...));
	}

	HandleImpl(const HandleImpl& other) = delete;
	HandleImpl& operator=(const HandleImpl& other) = delete;

	HandleImpl(HandleImpl&& other) {
		*this = std::move(other);
	}

	HandleImpl& operator=(HandleImpl&& other) {
		Reset();
		_Valid = other._Valid;
		_Id = other._Id;

		other._Valid = false;
		other._Id = 0;

		return *this;
	}

	GLuint Id() const {
		return _Id;
	}

	GLuint operator*() const {
		return Id();
	}

	void Reset() {
		_Valid = false;
		Resource::Delete(_Id);
		_Id = 0;
	}

private:
	GLuint _Id = 0;
	bool _Valid = false;

};

struct SamplerResource {
	static GLuint Create() {
		GLuint id;
		glCreateSamplers(1, &id);
		return id;
	}
	static void Delete(GLuint id) {
		glDeleteSamplers(1, &id);
	}
};

struct BufferResource {
	static GLuint Create() {
		GLuint id;
		glCreateBuffers(1, &id);
		return id;
	}
	static void Delete(GLuint id) {
		glDeleteBuffers(1, &id);
	}
};

struct VertexArrayResource {
	static GLuint Create() {
		GLuint id;
		glCreateVertexArrays(1, &id);
		return id;
	}
	static void Delete(GLuint id) {
		glDeleteVertexArrays(1, &id);
	}
};

struct ProgramResource {
	static GLuint Create() {
		return glCreateProgram();
	}

	static void Delete(GLuint id) {
		glDeleteProgram(id);
	}
};

struct ShaderResource {
	static GLuint Create(GLenum type) {
		return glCreateShader(type);
	}
	static void Delete(GLuint id) {
		glDeleteShader(id);
	}
};

struct TextureResource {
	static GLuint Create(GLenum target) {
		GLuint id;
		glCreateTextures(target, 1, &id);
		return id;
	}
	static void Delete(GLuint id) {
		glDeleteTextures(1, &id);
	}
};

struct FramebufferResource {
	static GLuint Create() {
		GLuint id;
		glCreateFramebuffers(1, &id);
		return id;
	}
	static void Delete(GLuint id) {
		glDeleteFramebuffers(1, &id);
	}
};

struct RenderBufferResource {
	static GLuint Create() {
		GLuint id;
		glCreateRenderbuffers(1, &id);
		return id;
	}
	static void Delete(GLuint id) {
		glDeleteRenderbuffers(1, &id);
	}
};


using Texture = HandleImpl<TextureResource>;
using Framebuffer = HandleImpl<FramebufferResource>;
using Buffer = HandleImpl<BufferResource>;
using Shader = HandleImpl<ShaderResource>;
using Program = HandleImpl<ProgramResource>;
using VertexArray = HandleImpl<VertexArrayResource>;
using Sampler = HandleImpl<SamplerResource>;
using RenderBuffer = HandleImpl<RenderBufferResource>;
}

namespace Shader {
enum class Type {
	Vertex, Fragment, Geometry
};

struct Source {
	Shader::Type Type;
	std::string Src;

	Source(Shader::Type type, std::string source) :
			Type(type), Src(std::move(source)) {

	}
};

namespace Detail {
inline GLenum GetShaderType(Type type) {
	if (type == Type::Vertex)
		return GL_VERTEX_SHADER;
	else if (type == Type::Fragment)
		return GL_FRAGMENT_SHADER;
	else if (type == Type::Geometry)
		return GL_GEOMETRY_SHADER;

	return -1;
}

template<class Handle, class GetIv, class GetInfoLog>
inline void Check(const Handle& handle, int statusName, GetIv getIvFunc,
		GetInfoLog getInfoLogFunc) {
	auto status = 0;
	getIvFunc(*handle, statusName, &status);

	if (!status) {
		auto length = 0;
		getIvFunc(*handle, GL_INFO_LOG_LENGTH, &length);

		if (length == 0)
			return;

		std::vector<char> buffer;
		buffer.resize(length);
		getInfoLogFunc(*handle, length, &length, buffer.data());

		std::string log(std::begin(buffer), std::end(buffer));

		throw std::runtime_error(log);
	}
}
}

inline Handle::Program Compile(const std::vector<Source>& shaders) {
	auto program = Handle::Program::Create();

	for (auto& shader : shaders) {
		auto shaderHandle = Handle::Shader::Create(Detail::GetShaderType(shader.Type));

		GLint length = shader.Src.length();
		auto src = shader.Src.c_str();
		glShaderSource(*shaderHandle, 1, &src, &length);
		glCompileShader(*shaderHandle);

		Detail::Check(shaderHandle, GL_COMPILE_STATUS,
		glGetShaderiv, glGetShaderInfoLog);

		glAttachShader(*program, *shaderHandle);
	}

	glLinkProgram(*program);
	Detail::Check(program, GL_LINK_STATUS,
	glGetProgramiv, glGetProgramInfoLog);

	return program;
}
}

namespace Error {
inline std::string ToString(GLenum error)
{
    switch (error) {
    case GL_NO_ERROR:
        return "GL_NO_ERROR";
        break;
    case GL_INVALID_ENUM:
        return "GL_INVALID_ENUM";
        break;
    case GL_INVALID_VALUE:
        return "GL_INVALID_VALUE";
        break;
    case GL_INVALID_OPERATION:
        return "GL_INVALID_OPERATION";
        break;
    case GL_INVALID_FRAMEBUFFER_OPERATION:
        return "GL_INVALID_FRAMEBUFFER_OPERATION";
        break;
    case GL_OUT_OF_MEMORY:
        return "GL_OUT_OF_MEMORY";
        break;
    case GL_STACK_UNDERFLOW:
        return "GL_STACK_UNDERFLOW";
        break;
    case GL_STACK_OVERFLOW:
        return "GL_STACK_OVERFLOW";
        break;
    }

    return "";
}


inline void Check()
{
    auto error = glGetError();

    if(error != GL_NO_ERROR) {
        throw std::runtime_error(ToString(error));
    }
}
}
}
