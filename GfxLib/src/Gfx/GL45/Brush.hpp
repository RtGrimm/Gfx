#pragma once

#include <array>
#include <glm/glm.hpp>
#include "RendererConst.hpp"
#include "../Union.hpp"

namespace Gfx
{
namespace GL45
{
enum class BrushType
{
	SolidColor = 1, RadialGradient = 2, LinearGradient = 3
};

struct SolidColorData
{
	glm::vec4 Color;
	static constexpr BrushType Type = BrushType::SolidColor;

	SolidColorData(glm::vec4 color) :
			Color(color)
	{
	}
};

struct GradientStopData
{
	std::array<float, RendererConst::MaxGradientStops> StopOffsets;
	std::array<glm::vec4, RendererConst::MaxGradientStops> StopColors;

	int StopCount;
	glm::vec3 _Padding_;
};

struct RadialGradientData
{
	static constexpr BrushType Type = BrushType::RadialGradient;

	glm::vec2 Center;
	glm::vec2 Radius;

	GradientStopData StopData;

};

struct LinearGradientData
{
	static constexpr BrushType Type = BrushType::LinearGradient;

	glm::vec2 Start;
	glm::vec2 End;

	GradientStopData StopData;

};

using Brush = Union<SolidColorData, RadialGradientData, LinearGradientData>;

}
}
