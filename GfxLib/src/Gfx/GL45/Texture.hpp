#pragma once
#include <glm/glm.hpp>
#include "GLUtils.hpp"

namespace Gfx
{
namespace GL45
{
class Texture
{

public:

	Texture(GL::Handle::Texture handle, uint32_t width, uint32_t height) :
			_Handle(std::move(handle)), _Width(width), _Height(height)
	{
		_BindlessHandle = glGetTextureHandleARB(_Handle.Id());
		MakeResident();
	}

	~Texture() {
		MakeNonResident();
	}

	void MakeResident()
	{
		glMakeTextureHandleResidentARB(_BindlessHandle);
		_IsResident = true;
	}

	void MakeNonResident()
	{
		glMakeTextureHandleNonResidentARB(_BindlessHandle);
		_IsResident = false;
	}

	GLuint64 BindlessHandle() const
	{
		return _BindlessHandle;
	}

	const GL::Handle::Texture& Handle() const
	{
		return _Handle;
	}

	bool IsResident() const
	{
		return _IsResident;
	}

private:

	GL::Handle::Texture _Handle;
	uint32_t _Width = 0;
	uint32_t _Height = 0;

	bool _IsResident = false;
	GLuint64 _BindlessHandle = 0;
};

using TextureHandleList = HandleList<TextureTag, Texture, IncIdController>;

class TextureResourceManager
{
public:
	TextureResourceManager()
	{

	}

	TextureHandle Create(const TextureDesc& desc)
	{
		auto textureHandle = GL::Handle::Texture::Create(GL_TEXTURE_2D);

		glTextureStorage2D(textureHandle.Id(), 1, GL_RGBA32F, desc.Width,
				desc.Height);

		glTextureSubImage2D(textureHandle.Id(), 0, 0, 0, desc.Width,
				desc.Height,
				GL_BGRA, GL_UNSIGNED_BYTE, desc.Data.Data());

		glTextureParameteri(textureHandle.Id(), GL_TEXTURE_MIN_FILTER,
		GL_LINEAR);
		glTextureParameteri(textureHandle.Id(), GL_TEXTURE_MAG_FILTER,
		GL_LINEAR);

		return _HandleList.Create(std::move(textureHandle), desc.Width,
				desc.Height);
	}

	TextureHandleList& HandleList()
	{
		return _HandleList;
	}

	const TextureHandleList& HandleList() const
	{
		return _HandleList;
	}

private:
	TextureHandleList _HandleList;
};
}
}
