#pragma once

#include <cstdint>

namespace Gfx {
namespace GL45 {
struct RendererConst
{
	static constexpr uint32_t TransformBindingPoint = 1;
	static constexpr uint32_t BrushBindingPoint = 2;

	static constexpr uint32_t MaxGradientStops = 16;
};

}
}
