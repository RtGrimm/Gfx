#pragma once
#include "BufferControllers.hpp"
#include <unordered_set>
#include <iostream>

namespace Gfx
{
namespace GL45
{
template<class HandleList, class T>
struct DefaultHandleSelector
{
	static const T& Select(const HandleList& handleList, const HandleIdType& id)
	{
		return handleList.Resolve(id);
	}
};

template<class T, class HandleList, class HandleRef,
		class HandleDataSelector = DefaultHandleSelector<HandleList, T>>
class ItemBuffer
{
public:

	ItemBuffer(const HandleList& handleList) :
			_HandleList(handleList), _Buffer(10)
	{

	}

	void Upload(const std::vector<HandleRef>& handles)
	{
		EnsureSize(handles.size());

		std::unordered_set<HandleIdType> uploadHandles;

		for (auto& handle : handles)
		{
			uploadHandles.insert(std::end(_CurrentItems), handle.Id());
		}

		auto it = std::begin(_CurrentItems);

		while (it != std::end(_CurrentItems))
		{
			auto currentHandle = *it;

			if (uploadHandles.find(currentHandle) == std::end(uploadHandles))
			{
				it = _CurrentItems.erase(it);

				auto ptr = _OffsetMap[currentHandle];

				_Buffer.Free(ptr);
				_OffsetMap.erase(_OffsetMap.find(currentHandle));

				if (it == std::end(_CurrentItems))
					break;

			}
			else
			{
				uploadHandles.erase(currentHandle);
				it++;
			}
		}

		UploadItems(uploadHandles);
	}

	uint32_t Resolve(HandleRef handle) const
	{
		return _OffsetMap.at(handle.Id());
	}

	const GL::Handle::Buffer& Buffer() const
	{
		return _Buffer.Handle();
	}

private:
	const HandleList& _HandleList;

	ItemBufferController<SingleSizeAllocator, T> _Buffer;
	std::unordered_set<HandleIdType> _CurrentItems;
	std::unordered_map<HandleIdType, uint32_t> _OffsetMap;

	void UploadItems(const std::unordered_set<HandleIdType>& handles)
	{
		_Buffer.Map();

		for (auto& id : handles)
		{
			const auto& value = HandleDataSelector::Select(_HandleList, id);

			auto offset = _Buffer.Alloc();
			_OffsetMap[id] = offset;

			auto ptr = _Buffer.Resolve(offset);
			_CurrentItems.insert(id);

			*ptr = value;
		}

		_Buffer.Unmap();
	}

	void EnsureSize(uint32_t count)
	{
		if (_Buffer.MaxCount() < count)
		{
			_Buffer.Resize(count);
		}
	}
};
}
}
