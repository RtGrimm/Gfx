#pragma once
#include <memory>
#include <glm/glm.hpp>
#include <stdint.h>
#include <vector>
#include <array>
#include "HandleList.hpp"
#include "View.hpp"

namespace Gfx
{

struct TextureTag
{
};

struct RenderTargetTag
{
};

struct MeshTag
{
};

struct BrushTag
{
};

struct TransformTag
{
};

struct Vertex
{
	glm::vec3 Location;
	glm::vec2 UV;

	Vertex(glm::vec3 location, glm::vec2 uv) :
			Location(location), UV(uv)
	{

	}
};

using RenderTargetHandle = Handle<RenderTargetTag>;
using MeshHandle = Handle<MeshTag>;
using BrushHandle = Handle<BrushTag>;
using TransformHandle = Handle<TransformTag>;
using TextureHandle = Handle<TextureTag>;

using RenderTargetRef = HandleRef<RenderTargetTag>;
using MeshRef = HandleRef<MeshTag>;
using BrushRef = HandleRef<BrushTag>;
using TransformRef = HandleRef<TransformTag>;
using TextureRef = HandleRef<TextureTag>;

struct RenderTargetDesc
{
	uint32_t Width = 0;
	uint32_t Height = 0;
};

struct TextureDesc
{
	uint32_t Width = 0;
	uint32_t Height = 0;

	ConstView<uint8_t> Data;
};

struct RenderState
{
	glm::vec4 ClearColor;
	glm::vec2 Size;
	RenderTargetRef Target;
};

struct Mesh
{
	std::vector<Vertex> Vertices;
	std::vector<uint32_t> Indices;

	Mesh()
	{
	}

	Mesh(std::vector<Vertex> vertices, std::vector<uint32_t> indices) :
			Vertices(vertices), Indices(indices)
	{
	}
};

struct GradientStop
{
	glm::vec4 Color;
	float Offset = 0;
};

struct Gradient
{
	std::vector<GradientStop> Stops;
};

struct LinearGradient: public Gradient
{
	glm::vec2 Start;
	glm::vec2 End;
};

struct RadialGradient: public Gradient
{
	glm::vec2 Center;
	glm::vec2 Radius;
};

struct IResourceFactory
{
	virtual ~IResourceFactory()
	{
	}

	virtual RenderTargetRef GetScreenRenderTarget() = 0;

	virtual RenderTargetHandle CreateRenderTarget(
			const RenderTargetDesc& desc) = 0;
	virtual TextureHandle CreateTexture(const TextureDesc& desc) = 0;

	virtual MeshHandle CreateMesh(Mesh& desc) = 0;
	virtual BrushHandle CreateSolidBrush(glm::vec4 color) = 0;
	virtual TransformHandle CreateTransform(const glm::mat4& transform) = 0;
	virtual BrushHandle CreateRadialGradientBrush(
			const RadialGradient& gradient) = 0;

	virtual BrushHandle CreateLinearGradientBrush(
			const LinearGradient& gradient) = 0;
};

struct ICommandList
{
	virtual ~ICommandList()
	{
	}



	virtual void DrawMesh(MeshRef mesh, BrushRef brush,
			TransformRef transform) = 0;

	virtual void DrawImage(TextureRef image, TransformRef transform) = 0;

	virtual void Clear() = 0;
};

struct ICommandRunner
{
	virtual ~ICommandRunner()
	{
	}
	virtual void Run(const RenderState& renderState,
			const ICommandList& commandList) = 0;
};

struct IPlatform
{
	virtual ~IPlatform()
	{
	}
	virtual ICommandRunner& GetCommandRunner() = 0;
	virtual std::unique_ptr<ICommandList> CreateCommandList() = 0;
	virtual IResourceFactory& GetResourceFactory() = 0;
};

}
