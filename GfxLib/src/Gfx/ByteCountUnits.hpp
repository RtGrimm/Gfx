#pragma once
#include <cstdint>

namespace Gfx {

template<class T, class Tag>
class NumericWrapper {
public:
	using This = NumericWrapper<T, Tag>;
	using Type = T;

	explicit NumericWrapper(T value) :
			_Value(value) {

	}

	NumericWrapper() {
	}

	T Get() {
		return _Value;
	}

	bool operator<(This other) {
		return _Value < other.Get();
	}

	bool operator>(This other) {
		return _Value > other.Get();
	}

	bool operator<=(This other) {
		return _Value <= other.Get();
	}

	bool operator>=(This other) {
		return _Value >= other.Get();
	}

	bool operator==(This other) {
		return _Value == other.Get();
	}

	bool operator<(T other) {
		return _Value < other;
	}

	bool operator>(T other) {
		return _Value > other;
	}

	bool operator==(T other) {
		return _Value == other;
	}

	This operator^(This other) {

		return _Value;
	}

	T operator--(int) {
		_Value--;
		return _Value;
	}

	T operator++(int) {
		_Value++;
		return _Value;
	}

	T operator--() {
		auto lastValue = _Value;
		_Value--;
		return lastValue;
	}

	T operator++() {
		auto lastValue = _Value;
		_Value++;
		return lastValue;
	}

	T operator+(T other) {
		return _Value + other;
	}

	T operator-(T other) {
		return _Value - other;
	}

	T operator*(T other) {
		return _Value * other;
	}

	T operator/(T other) {
		return _Value / other;
	}

	This operator+(This other) {
		return This(_Value + other.Get());
	}

	This operator-(This other) {
		return This(_Value - other.Get());
	}

	This operator*(This other) {
		return This(_Value * other.Get());
	}

	This operator/(This other) {
		return This(_Value / other.Get());
	}

	This operator-() {
		return This(-_Value);
	}

private:
	T _Value = 0;
};

struct BytesTag {
};

using ByteValue = NumericWrapper<int32_t, BytesTag>;

struct CountTag {
};

template<class T>
using CountValue = NumericWrapper<int32_t, CountTag>;

ByteValue Bytes(int32_t bytes) {
	return ByteValue(bytes);
}

template<class T>
CountValue<T> Count(int32_t count) {
	return CountValue<T>(count);
}

template<class T>
CountValue<T> BytesToCount(ByteValue bytes) {
	return CountValue<T>(bytes.Get() / sizeof(T));
}

template<class T>
CountValue<T> CountToBytes(CountValue<T> count) {
	return Bytes(count.get() * sizeof(T));
}
}
