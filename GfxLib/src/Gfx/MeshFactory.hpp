#pragma once
#include <memory>
#include "Gfx.hpp"
#include <algorithm>
#include "Path.hpp"

namespace Gfx
{


class MeshFactory
{
public:
	MeshFactory(IResourceFactory& resourceFactory);
	~MeshFactory();

	MeshHandle BuildMesh(const Path& path);

private:
	struct InternalData;
	std::unique_ptr<InternalData> _Data;

	IResourceFactory& _ResourceFactory;

	template<class It>
	glm::vec4 BoundingBox(It pointBegin, It pointEnd) const
	{
		auto floatMax = std::numeric_limits<float>::max();
		auto floatMin = std::numeric_limits<float>::min();

		float xMin = floatMax, yMin = floatMax;
		float xMax = floatMin, yMax = floatMin;

		auto min = [] (float x, float y)
		{
			return x > y ? y : x;
		};

		auto max = [] (float x, float y)
		{
			return x < y ? y : x;
		};

		std::for_each(pointBegin, pointEnd, [&] (auto& point)
		{
			xMin = min(xMin, point.x);
			yMin = min(yMin, point.y);

			xMax = max(xMax, point.x);
			yMax = max(yMax, point.y);
		});

		return glm::vec4(xMin, yMin, xMax - xMin, yMax - yMin);
	}

	template<class It>
	std::vector<Vertex> GenerateVertices(It pointBegin, It pointEnd) const
	{
		std::vector<Vertex> vertices;
		vertices.reserve(std::distance(pointBegin, pointEnd));

		auto boundingBox = BoundingBox(pointBegin, pointEnd);

		std::for_each(pointBegin, pointEnd, [&] (const glm::vec3& point)
		{

			auto localPoint = glm::vec2(point.x, point.y) -
			glm::vec2(boundingBox.x, boundingBox.y);

			auto uv = localPoint / glm::vec2(boundingBox.z, boundingBox.w);

			vertices.emplace_back(point, uv);
		});

		return vertices;
	}

};
}
