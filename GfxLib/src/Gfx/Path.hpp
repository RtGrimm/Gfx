#pragma once
#include <vector>
#include <glm/glm.hpp>
#include "Spline.hpp"

namespace Gfx
{
struct Polyline
{
	std::vector<glm::vec2> Points;
	Polyline(std::vector<glm::vec2> points) :
			Points(std::move(points))
	{

	}

	Polyline()
	{
	}

	Polyline(Polyline&&) = default;
	Polyline& operator=(Polyline&&) = default;

	Polyline(const Polyline&) = default;
	Polyline& operator=(const Polyline&) = default;
};

struct Path
{
	std::vector<Polyline> Figures;

	Path(std::vector<Polyline> figures) :
			Figures(std::move(figures))
	{

	}

	Path()
	{
	}

	Path(Path&&) = default;
	Path& operator=(Path&&) = default;

	Path(const Path&) = default;
	Path& operator=(const Path&) = default;
};

class PathBuilder
{
public:
	PathBuilder()
	{
	}

	Path Get()
	{
		NewFigure();
		return std::move(_Path);
	}

	void NewFigure()
	{
		if (_CurrentPolyline.Points.size() > 0)
			_Path.Figures.push_back(std::move(_CurrentPolyline));
	}

	void Figure(Polyline polyline)
	{
		_Path.Figures.push_back(polyline);
	}

	void Ellipse(glm::vec4 rect, uint32_t pointCount)
	{
		std::vector<glm::vec2> points;

		float segmentAngle = (2.0f * M_PI) / static_cast<float>(pointCount);

		for (uint32_t i = 1; i <= pointCount; i++)
		{
			auto angle = segmentAngle * i;

			auto x = rect.x + cosf(angle) * rect.z;
			auto y = rect.y + sinf(angle) * rect.w;

			points.emplace_back(x, y);
		}

		Figure(std::move(points));
	}

	void Rectangle(glm::vec4 rect)
	{
		std::vector<glm::vec2> points
		{
		{ rect.x, rect.y },
		{ rect.x + rect.z, rect.y },
		{ rect.x + rect.z, rect.y + rect.w },
		{ rect.x, rect.y + rect.w } };

		Figure(std::move(points));
	}

	void Bezier(const std::vector<glm::vec2>& points, float delta)
	{
		for (float i = 0; i < 1; i += delta)
		{
			_CurrentPolyline.Points.push_back(Spline::BezierCurve(points, i));
		}
	}

	void Points(const std::vector<glm::vec2>& points)
	{
		_CurrentPolyline.Points.insert(std::end(_CurrentPolyline.Points),
				std::begin(points), std::end(points));
	}

	void Point(glm::vec2 point)
	{
		_CurrentPolyline.Points.push_back(point);
	}

private:
	Polyline _CurrentPolyline;
	Path _Path;
};
}
