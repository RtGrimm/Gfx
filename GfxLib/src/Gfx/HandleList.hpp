#pragma once
#include <unordered_map>

namespace Gfx {


using HandleIdType = int32_t;

struct IHandleDestructor {
	virtual ~IHandleDestructor() {
	}
	virtual void Delete(HandleIdType id) = 0;
};

namespace Detail {
inline bool IsValid(HandleIdType id) {
	return id != -1;
}

inline void CheckId(HandleIdType id) {
	if (!IsValid(id)) {
		throw std::runtime_error("Invalid handle.");
	}
}
}

template<class Tag>
class HandleRef {
public:
	HandleRef(HandleIdType id) :
			_Id(id) {
	}

	HandleRef() :
			_Id(-1) {
	}

	HandleIdType Id() const {
		Detail::CheckId(_Id);
		return _Id;
	}

	bool IsValid() const {
		return Detail::IsValid(_Id);
	}

private:
	HandleIdType _Id;
};

template<class Tag>
class Handle {
public:
	Handle() {

	}

	Handle(HandleIdType id, IHandleDestructor* destructor) :
			_Id(id), _Destructor(destructor) {
	}

	Handle(const Handle&) = delete;
	Handle& operator=(const Handle&) = delete;

	Handle(Handle&& other) {
		*this = std::move(other);
	}

	Handle& operator=(Handle&& other) {
		Release();

		_Destructor = other._Destructor;
		_Id = other._Id;

		other._Destructor = nullptr;
		other._Id = -1;

		return *this;
	}

	operator HandleRef<Tag>() const {
		return Ref();
	}

	HandleRef<Tag> Ref() const {
		Detail::CheckId(_Id);
		return HandleRef<Tag>(_Id);
	}

	~Handle() {
		Release();
	}

	bool IsValid() const {
		Detail::CheckId(_Id);
		return Detail::IsValid(_Id);
	}

	HandleIdType Id() const {
		Detail::CheckId(_Id);
		return _Id;
	}

private:
	void Release() {
		if (_Id != -1) {
			_Destructor->Delete(_Id);
			_Id = -1;
			_Destructor = nullptr;
		}
	}

	HandleIdType _Id = -1;
	IHandleDestructor* _Destructor = nullptr;
};

template<class Tag, class Resource, class IdController>
class HandleList: public IHandleDestructor {
public:
	template<class ... Args>
	Handle<Tag> Create(Args&&... args) {
		auto id = _IdController.Create();
		_HandleMap.emplace(std::piecewise_construct, std::forward_as_tuple(id),
				std::tuple<Args&&...>(std::forward<Args>(args)...));

		return Handle<Tag>(id, this);
	}

	Resource& Resolve(HandleRef<Tag> ref) {
		return _HandleMap.at(ref.Id());
	}

	const Resource& Resolve(HandleRef<Tag> ref) const {
		return _HandleMap.at(ref.Id());
	}

	Resource& Resolve(HandleIdType id) {
		Detail::CheckId(id);
		return _HandleMap.at(id);
	}

	const Resource& Resolve(HandleIdType id) const {
		Detail::CheckId(id);
		return _HandleMap.at(id);
	}

	void Delete(HandleIdType id) override {
		_IdController.Delete(id);
		_HandleMap.erase(id);
	}

private:
	IdController _IdController;
	std::unordered_map<HandleIdType, Resource> _HandleMap;
};

class IncIdController {
public:

	HandleIdType Create() {
		_I++;
		return _I;
	}

	void Delete(HandleIdType) {

	}

private:
	int _I = 0;
};
}
