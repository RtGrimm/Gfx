#pragma once

#include "Path.hpp"

namespace Gfx
{
namespace PathOp
{
enum class JoinType
{
	Square, Round, Miter
};
enum class EndType
{
	ClosedPolygon, ClosedLine, OpenButt, OpenSquare, OpenRound
};

Path OffsetPath(const Path& path, float offset, JoinType joinType, EndType endType);
}
}
