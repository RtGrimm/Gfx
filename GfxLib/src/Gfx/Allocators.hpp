#pragma once
#include <list>
#include <cstdint>
#include <vector>
#include <cmath>
#include <exception>
#include <memory>


#include "ByteCountUnits.hpp"

namespace Gfx
{

template<class Unit>
class TreeBuddyAllocator
{
	struct Node
	{
		Unit Offset = 0;
		Unit Size = 0;
		Unit Order = 0;
		bool Free = true;
		Node* Parent = nullptr;

		Node(Unit offset, Unit order, bool free, Node* parent) :
				Offset(offset), Size(SizePow2(order)), Order(order), Free(free), Parent(
						parent)
		{

		}

		bool IsLeaf() const
		{
			return !Left && !Right;
		}

		std::unique_ptr<Node> Left;
		std::unique_ptr<Node> Right;
	};

public:
	void Resize(Unit size)
	{
		auto order = static_cast<Unit>(floor(log2(size)));
		_CurrentSize = SizePow2(order);
		_Root = std::make_unique<Node>(0, order, true, nullptr);
	}

	void Clear()
	{
		Resize(_CurrentSize);
	}

	Unit Alloc(Unit size)
	{
		auto node = Allocate(_Root.get(), size);

		if (node == nullptr) {
			throw std::bad_alloc();
		}


		return node->Offset;
	}

	void Free(Unit offset, Unit size)
	{
		auto freeNode = FindFreeNode(_Root.get(), offset, size);

		if (freeNode != nullptr)
		{
			MergeFreeNodes(freeNode);
		}
	}

	/*void PrintTree(std::string indent)
	{
		PrintTree(_Root.get(), indent);
	}*/

private:
	Unit _CurrentSize;
	std::unique_ptr<Node> _Root;

	/*void PrintTree(const Node* node, std::string indent)
	{
		if (node == nullptr)
			return;

		std::cout << indent << (node->IsLeaf() && node->Free ? "X" : "") << node->Offset << ":" << node->Size << std::endl;
		PrintTree(node->Left.get(), indent + indent);
		PrintTree(node->Right.get(), indent + indent);
	}*/

	Node* FindFreeNode(Node* root, Unit offset, Unit size)
	{
		if (root == nullptr)
			return nullptr;

		auto order = SizeLog2(size);

		if (root->Offset == offset && root->Order == order)
		{
			return root;
		}

		if (root->Right && offset >= root->Right->Offset)
		{
			return FindFreeNode(root->Right.get(), offset, size);
		}
		else
		{
			return FindFreeNode(root->Left.get(), offset, size);
		}

		return nullptr;
	}

	void MergeFreeNodes(Node* node)
	{
		if (node == nullptr)
			return;

		node->Free = true;

		if (node->Parent == nullptr)
			return;

		auto parent = node->Parent;

		auto left = (node == parent->Left.get() && parent->Right->IsLeaf()
				&& parent->Right->Free);
		auto right = (node == parent->Right.get() && parent->Left->IsLeaf()
				&& parent->Left->Free);

		if (left || right)
		{
			parent->Free = true;

			parent->Left.reset();
			parent->Right.reset();

			MergeFreeNodes(parent);
		}
	}

	Node* Allocate(Node* root, Unit size)
	{
		auto order = SizeLog2(size);

		if (root->Order < order)
			return nullptr;

		if (root->IsLeaf() && root->Free && root->Order == order)
		{
			root->Free = false;
			return root;
		}

		if (root->IsLeaf() && root->Free)
		{
			auto newPower = root->Order - 1;

			root->Left = std::make_unique<Node>(root->Offset, newPower, true,
					root);
			root->Right = std::make_unique<Node>(
					root->Offset + SizePow2(newPower), newPower, true, root);

			return Allocate(root->Left.get(), size);
		}

		Node* subNode = nullptr;

		if (root->Left)
			subNode = Allocate(root->Left.get(), size);

		if (subNode != nullptr)
			return subNode;

		if (root->Right)
		{
			subNode = Allocate(root->Right.get(), size);
		}

		return subNode;
	}

	static Unit SizePow2(Unit size)
	{
		return static_cast<Unit>(ceil(pow(2, size)));
	}

	static Unit SizeLog2(Unit size)
	{
		return static_cast<Unit>(ceil(log2(size)));
	}
};

template<class Unit>
class ListBuddyAllocator
{
public:
	ListBuddyAllocator(Unit size)
	{
		Resize(size);
	}

	ListBuddyAllocator()
	{

	}

	ListBuddyAllocator(const ListBuddyAllocator&) = delete;
	ListBuddyAllocator& operator=(const ListBuddyAllocator&) = delete;

	ListBuddyAllocator(ListBuddyAllocator&& other)
	{
		_FreeList = std::move(other._FreeList);
	}

	ListBuddyAllocator& operator=(ListBuddyAllocator&& other)
	{
		_FreeList = std::move(other._FreeList);
		return *this;
	}

	void Clear()
	{
		Resize(_CurrentSize);
	}

	void Resize(Unit size)
	{
		_FreeList.clear();

		auto power = floor(log2(size));

		if (power < 1)
			throw std::runtime_error(
					"Allocator size is Invalid. Size must be a power of two.");

		_FreeList.resize(power);
		_FreeList[power - 1].push_back(0);

		_CurrentSize = size;
	}

	Unit Alloc(Unit size)
	{

		Unit allocOrder = Unit(ceil(log2(size)));

		if (size == 0 || allocOrder == 0 || allocOrder > _FreeList.size())
			throw std::bad_alloc();

		Unit currentOrder = Unit(1);
		bool foundOrder = false;

		for (Unit i = Unit(_FreeList.size() - 1); i >= allocOrder - 1; i--)
		{
			if (_FreeList[i].size() > 0)
			{
				currentOrder = i + 1;
				foundOrder = true;
			}
		}

		if (!foundOrder)
			throw std::bad_alloc();

		if (currentOrder == allocOrder)
		{
			auto ptr = _FreeList[currentOrder - 1].front();
			_FreeList[currentOrder - 1].erase(
					std::begin(_FreeList[currentOrder - 1]));
			return ptr;
		}

		auto& fromList = _FreeList[currentOrder - 1];
		Unit ptr = *std::begin(fromList);
		fromList.erase(std::begin(fromList));

		while (currentOrder - 1 > allocOrder - 1)
		{
			auto& toList = _FreeList[currentOrder - 2];
			auto buddyPtr = ptr + Unit(pow(2, currentOrder - 1));

			toList.push_back(buddyPtr);

			currentOrder--;
		}

		return ptr;
	}

	void Free(Unit ptr, Unit size)
	{
		auto power = round(log2(size)) - 1;

		Unit i = Unit(0);
		for (i = power; i < _FreeList.size(); i++)
		{
			auto order = i + 1;
			Unit currentSize = Unit(pow(2, order));
			Unit offset = ptr;

			auto buddyPtr = (offset ^ currentSize);

			auto& freeList = _FreeList[i];

			auto it = std::begin(freeList);

			auto buddyFound = false;

			while (it != std::end(freeList))
			{
				if (*it == buddyPtr || (i == power && *it == ptr))
				{
					it = freeList.erase(it);
					buddyFound = true;

					if (it == std::end(freeList))
					{
						break;
					}
				}
				else
				{
					if (it != std::end(freeList))
						it++;
				}
			}

			if (buddyFound && buddyPtr < ptr)
			{
				ptr = buddyPtr;
			}

			if (!buddyFound)
			{
				break;
			}
		}

		_FreeList[i].push_back(ptr);
	}

private:
	Unit _CurrentSize;
	std::vector<std::vector<Unit>> _FreeList;
};

template<class Unit>
class SingleSizeAllocator
{
public:
	SingleSizeAllocator(Unit size)
	{
		Resize(size);
	}

	SingleSizeAllocator()
	{
	}

	void Resize(Unit size)
	{
		_FreeList.clear();

		auto count = size;
		_FreeList.reserve(count);

		for (Unit i = 0; i < count; ++i)
		{
			_FreeList.push_back(i);
		}

		_Size = size;
	}

	Unit Alloc()
	{
		EnsureAlloc();

		auto offset = _FreeList.back();
		_FreeList.erase(std::end(_FreeList) - 1);
		return offset;
	}

	void Free(Unit offset)
	{
		_FreeList.push_back(offset);
	}

	std::vector<Unit> Alloc(Unit count)
	{
		EnsureAlloc();

		auto start = std::begin(_FreeList);
		auto end = start + count;

		std::vector<Unit> offsets(start, end);
		_FreeList.erase(start, end);
		return offsets;
	}

private:
	void EnsureAlloc()
	{
		if (_FreeList.size() < 1)
			throw std::bad_alloc();
	}

	Unit _Size;
	std::vector<Unit> _FreeList;
};

}
