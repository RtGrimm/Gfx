#pragma once

#include <array>
#include <type_traits>

namespace Gfx
{

#define ENABLE_UNION_ERROR_CHECKING_AND_VALIDATION

namespace Detail
{
template<int X, int ... Xs>
struct Max_
{
	static constexpr int Value =
			X > Max_<Xs...>::Value ? X : Max_<Xs...>::Value;
};

template<int X>
struct Max_<X>
{
	static constexpr int Value = X;
};

template<int ... Xs>
constexpr int Max = Max_<Xs...>::Value;

template<class T, class X, class ... Xs>
struct ContainsType_
{
	static constexpr bool Value = ContainsType_<T, Xs...>::Value
			|| ContainsType_<T, X>::Value;
};

template<class T, class X>
struct ContainsType_<T, X>
{
	static constexpr bool Value = std::is_same<T, X>::value;
};

template<class T, class X, class ... Xs>
constexpr bool ContainsType = ContainsType_<T, X, Xs...>::Value;

template<class T, class X, class ... Xs>
struct IndexOf_
{
	static constexpr int Index =
			std::is_same<T, X>::value ? 0 : 1 + IndexOf_<T, Xs...>::Index;
};

template<class T>
struct IndexOf_<T, T>
{
	static constexpr int Index = 0;
};

template<class T, class X>
struct IndexOf_<T, X>
{
	static constexpr int Index = 0;
};

template<class T, class X, class ... Xs>
constexpr int IndexOf = IndexOf_<T, X, Xs...>::Index;

template<class T, class Callback>
class CaseItem
{
public:
	using Type = T;

	CaseItem(Callback callback) :
			_Callback(callback)
	{

	}

	void operator()(T&& input)
	{
		_Callback(std::forward<T>(input));
	}

	void operator()(const T&& input) const
	{
		_Callback(std::forward<const T>(input));
	}

private:
	Callback _Callback;
};
}

template<class T, class Callback>
Detail::CaseItem<T, Callback> Case(Callback callback)
{
	return Detail::CaseItem<T, Callback>(callback);
}

template<class X, class ... Xs>
class Union
{
	template<class T>
	using PureType = typename std::remove_cv<typename std::remove_reference<T>::type>::type;

public:

	template<class T>
	static constexpr int IndexOfType = Detail::IndexOf<PureType<T>, X, Xs...>;

	template<class T>
	static constexpr bool ContainsType = Detail::ContainsType<PureType<T>, X, Xs...>;



	using This = Union<X, Xs...>;

	static constexpr int Size = Detail::Max<sizeof(X), sizeof(Xs)...>;

	Union()
	{
	}

	template<class T>
	Union(T&& value,
			typename std::enable_if<ContainsType<T>, int>::type =
					0)
	{
		Construct(std::forward<T>(value));
	}

	Union(This&& other)
	{
		*this = std::move(other);
	}

	This& operator=(This&& other)
	{
		Release();

		_Index = other._Index;
		_Data = other._Data;

		other.Release();

		return *this;
	}

	Union(const This& other)
	{
		*this = other;
	}

	This& operator=(const This& other)
	{
		Release();

		_Index = other._Index;
		_Data = other._Data;

		return *this;
	}

	int Index() const
	{
		return _Index;
	}

	void Match() const
	{
	}

	template<class Case, class ... Cases>
	void Match(const Case&& _case, const Cases&& ... cases) const
	{
		if (Is<typename Case::Type>())
		{
			_case(
					std::forward<const typename Case::Type>(
							Get<const typename Case::Type>()));
		}
		else
		{
			Match(std::forward<const Cases>(cases)...);
		}
	}

	template<class Case, class ... Cases>
	void Match(Case&& _case, Cases&& ... cases)
	{
		if (Is<typename Case::Type>())
		{
			_case(
					std::forward<typename Case::Type>(
							Get<typename Case::Type>()));
		}
		else
		{
			Match(std::forward<Cases>(cases)...);
		}
	}

	template<class T>
	void Construct(T&& value)
	{

		using Type = typename std::remove_reference<T>::type;

		AssertTypeMembership<Type>();
		Release();
		new (_Data.data()) Type(std::forward<T>(value));
		_Index = Detail::IndexOf<Type, X, Xs...>;
	}

	template<class T, class ... Args>
	void Construct(Args&&... args)
	{
		AssertTypeMembership<T>();

		Release();
		new (_Data.data()) T(std::forward<Args>(args)...);
		_Index = Detail::IndexOf<T, X, Xs...>;
	}

	template<class T>
	T& Get()
	{
		AssertTypeMembership<T>();
		CheckValidity();
		CheckTypeMatch<T>();

		return *reinterpret_cast<T*>(_Data.data());
	}

	template<class T>
	const T& Get() const
	{
		AssertTypeMembership<T>();
		CheckValidity();
		CheckTypeMatch<T>();

		return *reinterpret_cast<const T*>(_Data.data());
	}

	template<class T>
	bool Is() const
	{
		return _Index == Detail::IndexOf<PureType<T>, X, Xs...>;
	}

	~Union()
	{
		Release();
	}

private:

	void Release()
	{
		if (_Index != -1)
		{
			Release_<X, Xs...>::Run(*this);

			_Index = -1;
		}
	}

	template<class Y, class ... Ys>
	struct Release_
	{
		static void Run(This& thisRef)
		{
			if (Detail::IndexOf<Y, X, Xs...> == thisRef._Index)
			{
				Release_<Y>::Run(thisRef);
			}
			else
			{
				Release_<Ys...>::Run(thisRef);
			}
		}
	};

	template<class Y>
	struct Release_<Y>
	{
		static void Run(This& thisRef)
		{
			thisRef.Get<Y>().~Y();
		}
	};

	template<class T>
	constexpr void AssertTypeMembership() const
	{
		static_assert(ContainsType<T>,
				"Type is not present in union.");
	}

	template<class T>
	void CheckTypeMatch() const
	{
#ifdef ENABLE_UNION_ERROR_CHECKING_AND_VALIDATION
		if (!Is<T>())
			throw std::runtime_error("Type mismatch.");
#endif
	}

	void CheckValidity() const
	{
#ifdef ENABLE_UNION_ERROR_CHECKING_AND_VALIDATION
		if (_Index == -1)
			throw std::runtime_error("Union is uninitialized.");
#endif
	}

	int _Index = -1;
	std::array<char, Size> _Data;
};
}
