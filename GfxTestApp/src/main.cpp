#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include "Gfx/Gfx.hpp"
#include "Gfx/GL45/GL45.hpp"
#include "Gfx/MeshFactory.hpp"
#include "Gfx/Path.hpp"
#include "Gfx/PathOp.hpp"

#include "Gfx/Union.hpp"

#include <memory>
#include <iostream>
#include <chrono>
#include <FreeImage.h>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

template<class Callback>
std::chrono::milliseconds Clock(Callback callback)
{
	auto start = std::chrono::system_clock::now();

	callback();

	auto end = std::chrono::system_clock::now();

	return std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
}

class Image
{
public:
	Image(std::string path)
	{
		Load(path);
	}

	~Image()
	{
		FreeImage_Unload(_Bitmap);
	}

	Gfx::ConstView<uint8_t> View() const
	{
		return Gfx::ConstView<uint8_t>(FreeImage_GetBits(_Bitmap),
				Width() * Height() * (FreeImage_GetBPP(_Bitmap) / 8));
	}

	int32_t Height() const
	{
		return FreeImage_GetHeight(_Bitmap);
	}

	int32_t Width() const
	{
		return FreeImage_GetWidth(_Bitmap);
	}

private:
	void Load(std::string path)
	{
		auto format = FreeImage_GetFileType(path.c_str(), 0);
		auto bitmap = FreeImage_Load(format, path.c_str());

		_Bitmap = FreeImage_ConvertTo32Bits(bitmap);

		FreeImage_Unload(bitmap);
	}

	FIBITMAP* _Bitmap;
};

class App
{
public:
	App()
	{

	}

	void Start()
	{

		glfwInit();

		glfwWindowHint(GLFW_SAMPLES, 8);
		glfwWindowHint(GLFW_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_VERSION_MINOR, 5);

		_Window = glfwCreateWindow(1200, 800, "MainWindow", nullptr, nullptr);
		glfwMakeContextCurrent(_Window);

		glewExperimental = true;
		glewInit();

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		glDebugMessageCallback([] (GLenum source,
				GLenum type,
				GLuint id,
				GLenum severity,
				GLsizei length,
				const GLchar* message,
				const void* userParam) -> void
		{
			std::cout << message << std::endl;
		}, nullptr);

		_Platform = Gfx::GL45::CreatePlatform();

		Run();

	}

private:
	std::unique_ptr<Gfx::IPlatform> _Platform;

	Gfx::TextureHandle LoadImage(std::string path)
	{
		Image image(path);

		Gfx::TextureDesc textureDesc;
		textureDesc.Data = image.View();
		textureDesc.Width = image.Width();
		textureDesc.Height = image.Height();

		auto texture = _Platform->GetResourceFactory().CreateTexture(
				textureDesc);

		return texture;
	}

	std::vector<Gfx::TextureHandle> LoadImages(std::string path)
	{
		fs::directory_iterator start(path);
		fs::directory_iterator end;

		std::vector<Gfx::TextureHandle> textures;

		std::for_each(start, end, [&] (fs::path path)
		{
			textures.push_back(LoadImage(path.string()));
		});

		return textures;
	}

	void Run()
	{
		auto& commandRunner = _Platform->GetCommandRunner();

		auto images = LoadImages(
				"/home/ryan/Documents/IdeaProjects/BingImageDownloader/Images");

		Gfx::MeshFactory meshFactory(_Platform->GetResourceFactory());
		Gfx::PathBuilder builder;

		builder.Ellipse(glm::vec4(100, 100, 100, 100), 50);
		auto mesh = meshFactory.BuildMesh(builder.Get());

		Gfx::LinearGradient gradient;
		gradient.Start = glm::vec2(0.3f, 0.3f);
		gradient.End = glm::vec2(0.7f, 0.7f);
		gradient.Stops =
		{
			{	glm::vec4(1, 1, 0, 1), 0},
			{	glm::vec4(1, 1, 1, 1), 0.5f},
			{	glm::vec4(0, 1, 1, 1), 1.0f}
		};

		auto brush = _Platform->GetResourceFactory().CreateLinearGradientBrush(
				gradient);

		auto commandList = _Platform->CreateCommandList();
		auto target = _Platform->GetResourceFactory().GetScreenRenderTarget();

		commandList->Clear();

		std::vector<glm::vec2> points
		{
		{ 500, 500 },
		{ 600, 200 },
		{ 650, 600 } };

		while (!glfwWindowShouldClose(_Window))
		{

			std::cout
					<< Clock(
							[&]
							{
								int width = 0, height = 0;
								glfwGetWindowSize(_Window, &width, &height);

								auto projection = glm::ortho(0.0f, static_cast<float>(width),
										static_cast<float>(height), 0.0f);

								commandList->Clear();

								auto transform2 = _Platform->GetResourceFactory().CreateTransform(
										projection * glm::translate(glm::vec3(500, 500, 0)));

								commandList->DrawMesh(mesh, brush, transform2);

								std::vector<Gfx::TransformHandle> transforms;

								for(auto j = 0; j < 50; j++)
								{

									for(auto i = 0; i < 100; i++)
									{
										auto& image = images[i % images.size()];

										auto transform = _Platform->GetResourceFactory().CreateTransform(
												projection * glm::translate(glm::vec3(10 + i * 20, 10 + j * 20, 0)) * glm::scale(glm::vec3(20, 20, 1)));

										commandList->DrawImage(image, transform);

										transforms.push_back(std::move(transform));

										i++;
									}
								}

								Gfx::RenderState renderState
								{
									{	0, 0, 1, 1},
									{	width, height},
									target
								};

								commandRunner.Run(renderState, *commandList);
							}).count() << std::endl;

			glfwSwapBuffers(_Window);
			glfwPollEvents();
		}
	}

	GLFWwindow* _Window = nullptr;
};

int main()
{
	App app;
	app.Start();

	return 0;
}
